# FROM nginx:1.13.3-alpine
# COPY nginx/default.conf /etc/nginx/conf.d/
# RUN rm -rf /usr/share/nginx/html/*
# COPY build /usr/share/nginx/html
# EXPOSE 80
# CMD ["nginx", "-g", "daemon off;"]

# MULTI-STAGE:

FROM node:13.10.1-alpine3.11 as builder
RUN mkdir /ng-app
WORKDIR /ng-app
COPY . .
RUN npm ci
RUN npm run build

FROM nginx:1.13.3-alpine
COPY nginx/default.conf /etc/nginx/conf.d/
# Limpiar la carpeta HTML, los index.html y demás archivos por defecto de nginx
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /ng-app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
