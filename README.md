# React Search List

Search list made with ReactJS, cool transitions implemented in Sass and containerized with Docker.

## Authors

* **Pablo Álamo** - [pablodevs](https://github.com/pablodevs)

### Docker Manual Installation:

1. `$ docker pull pablodevs/react-app:0.1.1`
2. `$ docker run -dp 3000:3000 pablodevs/react-app:0.1.1`
3. Open browser in http://localhost:3000

### Front-End Manual Installation:

1. `$ npm install`
2. `$ npm run start`

<div align="center">Handcrafted with ❤️ by [pablodevs](https://github.com/pablodevs)<div>