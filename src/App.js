import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';
import genKMembers from "./data.json";

const Tab = (props) => {
  return (
    <li onClick={() => props.toggleFunction(props.tabIndex)}>
      <div
        className={`corner corner--top ${
          props.activeTab === props.tabIndex ? "corner--show" : ""
        }`}
        />
      <div
        className={`corner ${
          props.activeTab === props.tabIndex ? "corner--show" : ""
        }`}
      />
      <button
        className={`tab ${
          props.activeTab === props.tabIndex ? " tab--active" : ""
        }`}
      >
        <div className="tab__avatar">
          <svg viewBox="0 0 24 24" className="tab__avatar__img">
            <path
              d="M12,3.5c2.347,0,4.25,1.903,4.25,4.25S14.347,12,12,12s-4.25-1.903-4.25-4.25S9.653,3.5,12,3.5z
                 M5,20.5
                 c0-3.866,3.134-7,7-7s7,3.134,7,7H5z"
            />
          </svg>
        </div>
        <span>{props.label}</span>
      </button>
    </li>
  );
};

Tab.propTypes = {
  label: PropTypes.string.isRequired,
  activeTab: PropTypes.number.isRequired,
  tabIndex: PropTypes.number.isRequired,
  toggleFunction: PropTypes.func.isRequired
};

const App = () => {
  const [searchInput, setSearchInput] = useState("");
  const [content, setContent] = useState("");
  const [list, setList] = useState([]);
  const [toggleState, setToggleState] = useState(-1);

  useEffect(() => {
    if (searchInput) {
      let newList = genKMembers.sort().map((name, index) => {
        if (name.toLowerCase().includes(searchInput.toLowerCase())) {
          return (
            <Tab
              key={`${name}_${index}`}
              label={name}
              activeTab={toggleState}
              tabIndex={index}
              toggleFunction={toggleTab}
            />
          );
        }
      });
      !newList.every((item) => item === undefined)
        ? setList(newList)
        : setList([
            <li
              key={`null`}
              style={{
                textAlign: "center",
                display: "block",
                paddingRight: "1rem"
              }}
            >
              No results found
            </li>
          ]);
        } else
      setList(
        genKMembers.sort().map((name, index) => (
          <Tab
            key={`${name}_${index}`}
            label={name}
            activeTab={toggleState}
            tabIndex={index}
            toggleFunction={toggleTab}
          />
        ))
      );
    }, [searchInput, toggleState]);

  const toggleTab = (tabIndex) => {
    setToggleState(tabIndex);
    setContent(genKMembers.sort()[tabIndex]);
  };

  const handleSearchOnChange = (e) => {
    setContent("");
    setToggleState(-1);
    setSearchInput(e.target.value);
  };

  return (
    <main className="main-container">
      <aside>
        <h1 class="title">GenK Members</h1>
        <div className="search">
          <input
            type="search"
            className="search__input"
            placeholder="enter your search"
            value={searchInput}
            onChange={handleSearchOnChange}
          />
          <FontAwesomeIcon icon={faMagnifyingGlass} />
        </div>
        <ul>{list}</ul>
      </aside>
      <div className={`content ${content ? "content--show" : ""}`}>
        {content}
      </div>
    </main>
  );
};

export default App;